import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity, JoinColumn, ManyToOne,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import { File } from "../../file/entities/file.entity";
import { Position } from "../../position/entities/position.entity";

@Entity("users")
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string

    @Column()
    lastName: string

    @Column()
    finCode: string

    @Column()
    phoneNumber: string

    @Column()
    email: string

    @Column()
    address: string

    @ManyToOne(_ => Position, d => d.users)
    position: Position;

    @Column({type: "simple-json"})
    serial: {prefix: string, number: string}

    @Column({ select: false })
    photoId: number;

    @OneToOne(_ => File, { cascade: true, primary: true })
    @JoinColumn({ name: "photoId" })
    photo: File;

    @CreateDateColumn()
    created!: Date;

    @UpdateDateColumn()
    updated!: Date;

    // Add this column to your entity!
    @DeleteDateColumn()
    deletedAt?: Date;
}
