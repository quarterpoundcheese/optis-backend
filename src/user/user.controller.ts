import { Controller, Get, Post, Body, Patch, Param, Delete, Put, UseInterceptors, UploadedFile } from "@nestjs/common";
import { UserService } from "./user.service";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { FileInterceptor } from "@nestjs/platform-express";

@Controller("user")
export class UserController {
    constructor(private readonly userService: UserService) {
    }

    @Post()
    @UseInterceptors(
        FileInterceptor("photo")
    )
    create(@UploadedFile() photo: Express.Multer.File, @Body() createUserDto: CreateUserDto) {
        return this.userService.create(photo, createUserDto);
    }

    @Get()
    findAll() {
        return this.userService.findAll();
    }

    @Get(":id")
    findOne(@Param("id") id: string) {
        return this.userService.findOne(+id);
    }

    @Put(":id")
    @UseInterceptors(
        FileInterceptor("photo")
    )
    update(@UploadedFile() photo: Express.Multer.File, @Param("id") id: string, @Body() updateUserDto: UpdateUserDto) {
        return this.userService.update(+id, photo, updateUserDto);
    }

    @Delete(":id")
    remove(@Param("id") id: string) {
        return this.userService.remove(+id);
    }
}
