import { BadRequestException, HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { Connection, Repository } from "typeorm";
import { Position } from "../position/entities/position.entity";
import { User } from "./entities/user.entity";
import { File } from "../file/entities/file.entity";
import { saveFile } from "../helpers/file.helper";
import { validate } from "class-validator";
import validationHelper from "../helpers/validation.helper";

@Injectable()
export class UserService {
    private _positionRepository: Repository<Position>;
    private _userRepository: Repository<User>;
    private _fileRepository: Repository<File>;

    constructor(private _connection: Connection) {
        this._positionRepository = this._connection.getRepository(Position);
        this._userRepository = this._connection.getRepository(User);
        this._fileRepository = this._connection.getRepository(File);
    }

    async create(photo: Express.Multer.File, createUserDto: CreateUserDto) {
        const fileName = await saveFile(photo);
        const userPhoto = this._fileRepository.create({
            originalName: photo.originalname,
            mimeType: photo.mimetype,
            path: fileName
        });

        await this._fileRepository.save(userPhoto);

        const position = await this._positionRepository.findOne(createUserDto.positionId);
        if (!position) throw new BadRequestException();

        const user = this._userRepository.create({ ...createUserDto, position: position, photo: userPhoto });
        const validationErrors = await validate(user);
        if (validationErrors.length > 0) throw new HttpException(validationHelper(validationErrors), HttpStatus.BAD_REQUEST);


        await this._userRepository.save(user);
        return user;
    }

    async findAll() {
        return await this._userRepository.createQueryBuilder("user")
            .select(["user", "position.id", "position.name", "department.id", "department.name", "company.id", "company.name"])
            .leftJoin("user.position", "position")
            .leftJoin("position.department", "department")
            .leftJoin("department.company", "company")
            .getRawAndEntities();
    }

    async findOne(id: number) {
        return await this._userRepository.createQueryBuilder("user")
            .select(["user", "position.id", "position.name", "department.id", "department.name", "company.id", "company.name"])
            .leftJoin("user.position", "position")
            .leftJoin("position.department", "department")
            .leftJoin("department.company", "company")
            .where({ id })
            .getOne();
    }

    async update(id: number, photo: Express.Multer.File, updateUserDto: UpdateUserDto) {
        let userPhoto;

        if (photo) {
            const fileName = await saveFile(photo);
            userPhoto = this._fileRepository.create({
                originalName: photo.originalname,
                mimeType: photo.mimetype,
                path: fileName
            });

            await this._fileRepository.save(userPhoto);
        }

        const user = await this._userRepository.findOne(id);
        if (!user) throw new BadRequestException();

        const position = await this._positionRepository.findOne(updateUserDto.positionId);
        if (!position) throw new BadRequestException();

        const updateUser = this._userRepository.create({
            ...user, ...updateUserDto,
            position: position,
            photo: photo ? userPhoto : user.photo
        });
        const validationErrors = await validate(updateUser);
        if (validationErrors.length > 0) throw new HttpException(validationHelper(validationErrors), HttpStatus.BAD_REQUEST);


        await this._userRepository.save(updateUser);
        return updateUser;
    }

    remove(id: number) {
        return `This action removes a #${id} user`;
    }
}
