export class CreateUserDto {
    id: number;

    firstName: string

    lastName: string

    phoneNumber: string

    email: string

    address: string

    positionId: number;

    serial: Serial
}

interface Serial {
    prefix: string
    number: string
}
