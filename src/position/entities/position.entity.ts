import {
    AfterLoad,
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity, getRepository,
    ManyToOne, OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import { Department } from "../../department/entities/department.entity";
import { IsNotEmpty, Length } from "class-validator";
import { User } from "src/user/entities/user.entity";


@Entity("positions")
export class Position {
    @PrimaryGeneratedColumn()
    id: number;

    @Length(2, 18)
    @IsNotEmpty()
    @Column()
    name: string;

    @ManyToOne(_ => Department, d => d.positions)
    department: Department;

    @OneToMany( _ => User, u => u)
    users: User[];

    @AfterLoad()
    async calculateDeletable() {
        const t = await getRepository(User).count({ where: { position: this } });
        this.isDeleteAble = t === 0;
    }

    isDeleteAble: boolean;

    @CreateDateColumn()
    created!: Date;

    @UpdateDateColumn()
    updated!: Date;

    // Add this column to your entity!
    @DeleteDateColumn()
    deletedAt?: Date;
}
