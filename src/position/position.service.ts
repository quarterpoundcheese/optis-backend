import { HttpException, HttpStatus, Injectable, NotFoundException } from "@nestjs/common";
import { CreatePositionDto } from "./dto/create-position.dto";
import { UpdatePositionDto } from "./dto/update-position.dto";
import { Connection, Repository } from "typeorm";
import { Position } from "./entities/position.entity";
import { Department } from "../department/entities/department.entity";
import { validate } from "class-validator";
import validationHelper from "../helpers/validation.helper";

@Injectable()
export class PositionService {
    private _positionRepository: Repository<Position>;
    private _departmentRepository: Repository<Department>;

    constructor(private _connection: Connection) {
        this._departmentRepository = this._connection.getRepository(Department);
        this._positionRepository = this._connection.getRepository(Position);
    }

    async create(createPositionDto: CreatePositionDto) {
        const validationErrors = await validate(createPositionDto);
        if (validationErrors.length > 0) throw new HttpException(validationHelper(validationErrors), HttpStatus.BAD_REQUEST);

        const department = await this._departmentRepository.findOne(createPositionDto.departmentId);
        if (!department) throw new NotFoundException();

        const position = this._positionRepository.create({ ...createPositionDto, department });
        await this._positionRepository.save(position);
        return position;
    }

    async findAll() {
        return await this._positionRepository.createQueryBuilder("position")
            .select(["position", "department.name", "department.id", "company.name", "company.id"])
            .leftJoin("position.department", "department")
            .leftJoin("department.company", "company")
            .getMany();
    }

    async findOne(id: number) {
        return await this._positionRepository.createQueryBuilder("position")
            .select(["position", "department.name", "department.id", "company.name", "company.id"])
            .leftJoin("position.department", "department")
            .leftJoin("department.company", "company")
            .where({ id })
            .getOne();
    }

    async update(id: number, updatePositionDto: UpdatePositionDto) {
        const validationErrors = await validate(updatePositionDto);
        if (validationErrors.length > 0) throw new HttpException(validationHelper(validationErrors), HttpStatus.BAD_REQUEST);

        const position = await this._positionRepository.findOne(id);
        if (!position) throw new NotFoundException();


        const department = await this._departmentRepository.findOne(updatePositionDto.departmentId);
        if (!department) throw new NotFoundException();

        const updatePosition = this._positionRepository.create({ ...position, ...updatePositionDto, department });
        await this._positionRepository.save(updatePosition);
        return position;
    }

    async remove(id: number) {
        return `This action removes a #${id} position`;
    }
}
