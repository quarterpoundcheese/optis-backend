import { IsNotEmpty, Length } from "class-validator";

export class CreatePositionDto {
    @Length(2, 36)
    @IsNotEmpty()
    name: string

    @IsNotEmpty()
    departmentId: number
}
