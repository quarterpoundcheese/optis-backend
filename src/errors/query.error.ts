import { QueryFailedError } from "typeorm";
import { ArgumentsHost, Catch, ExceptionFilter } from "@nestjs/common";
import { Response } from "express";

@Catch(QueryFailedError)
export class QueryErrorFilter implements ExceptionFilter {
    public catch(exception: any, host: ArgumentsHost): any {
        const detail = exception.detail;
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();

        if (typeof detail === "string" && detail.includes("already exists")) {
            if (detail.includes("email")) {
                return response.status(400).json({ email: ["TAKEN"] });
            }

            if (detail.includes("phoneNumber")) {
                return response.status(400).json({ phoneNumber: ["TAKEN"] });
            }
        }
    }
}