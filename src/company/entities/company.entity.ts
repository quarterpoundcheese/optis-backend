import {
    BeforeInsert,
    BeforeUpdate,
    Column, CreateDateColumn, DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    AfterLoad,
    OneToMany,
    OneToOne,
    PrimaryColumn, UpdateDateColumn, getRepository
} from "typeorm";
import { File } from "../../file/entities/file.entity";
import { IsEmail, IsNotEmpty, Length } from "class-validator";
import { Department } from "../../department/entities/department.entity";

@Entity("companies")
export class Company {
    @PrimaryColumn({ generated: true })
    id: number;

    @Length(2, 64, { message: "Şirkətin adının uzunluğu 2 və 64 hərf arası olmalıdır" })
    @IsNotEmpty({ message: "Boş qoyula bilməz" })
    @Column()
    name: string;

    @IsEmail()
    @IsNotEmpty({ message: "Boş qoyula bilməz" })
    @Column()
    email: string;

    @Length(2, 128, { message: "Şirkətin ünvanın uzunluğu 2 və 128 hərf arası olmalıdır" })
    @IsNotEmpty({ message: "Boş qoyula bilməz" })
    @Column()
    address: string;

    @Column({ select: false })
    photoId: number;


    @OneToOne(_ => File, { cascade: true, primary: true })
    @JoinColumn({ name: "photoId" })
    photo: File;

    @OneToMany(_ => Department, (department) => department.company, { cascade: true, onDelete: 'CASCADE', })
    departments: Department[];

    @AfterLoad()
    async calculateDeletable() {
        const t = await getRepository(Department).count({ where: { company: this } });
        this.isDeleteAble = t === 0;
    }
    isDeleteAble: boolean;

    @CreateDateColumn()
    created!: Date;

    @UpdateDateColumn()
    updated!: Date;

    // Add this column to your entity!
    @DeleteDateColumn()
    deletedAt?: Date;



    @BeforeUpdate()
    @BeforeInsert()
    trimStrings() {
        this.address = this.address.trim().toLocaleLowerCase();
        this.email = this.email.trim().toLocaleLowerCase();
        this.name = this.name.trim().toLocaleLowerCase();
    }


}
