import { BadRequestException, HttpException, HttpStatus, Injectable, NotFoundException } from "@nestjs/common";
import { CreateCompanyDto } from "./dto/create-company.dto";
import { UpdateCompanyDto } from "./dto/update-company.dto";
import { Connection, Repository } from "typeorm";
import { Company } from "./entities/company.entity";
import { saveFile } from "../helpers/file.helper";
import { File } from "../file/entities/file.entity";
import { validate } from "class-validator";
import validationHelper from "../helpers/validation.helper";

const ALLOWED_MIMETYPES = ["image/jpeg", "image/png"];


@Injectable()
export class CompanyService {

    private _companyRepository: Repository<Company>;
    private _fileRepository: Repository<File>;

    constructor(private _connection: Connection) {
        this._companyRepository = this._connection.getRepository(Company);
        this._fileRepository = this._connection.getRepository(File);
    }


    async create(photo: Express.Multer.File, createCompanyDto: CreateCompanyDto) {
        const fileName = await saveFile(photo);
        const companyPhoto = this._fileRepository.create({
            originalName: photo.originalname,
            mimeType: photo.mimetype,
            path: fileName
        });

        await this._fileRepository.save(companyPhoto);

        const company = this._companyRepository.create({ ...createCompanyDto, photo: companyPhoto });

        const validationErrors = await validate(company);
        if (validationErrors.length > 0) throw new HttpException(validationHelper(validationErrors), HttpStatus.BAD_REQUEST);


        await this._companyRepository.save(company);
        return company;
    }

    async findAll() {
        return await this._companyRepository.find({ relations: ["photo"] });
    }

    async findOne(id: number) {
        return await this._companyRepository.findOne({ relations: ["photo"], where: { id } });
    }

    async update(id: number, photo: Express.Multer.File, updateCompanyDto: UpdateCompanyDto) {

        const company = await this._companyRepository.findOne({ id }, {relations: ['photo']});

        if (!company) throw new NotFoundException();

        let companyPhoto;

        if (photo) {
            let fileName = await saveFile(photo, ALLOWED_MIMETYPES);
            companyPhoto = this._fileRepository.create({
                originalName: photo.originalname,
                mimeType: photo.mimetype,
                path: fileName
            });

            await this._fileRepository.save(companyPhoto);
        }

        const updatedCompany = this._companyRepository.create({
            ...company, ...updateCompanyDto,
            photo: !photo ? company.photo : companyPhoto
        });
        await this._companyRepository.save(updatedCompany);
        return updatedCompany;

    }


    async remove(id: number) {
        const company = await this._companyRepository.findOne(id);
        if(!company) throw new NotFoundException();
        if(!company.isDeleteAble) throw new BadRequestException();

        await this._companyRepository.softDelete({id: company.id})
    }
}
