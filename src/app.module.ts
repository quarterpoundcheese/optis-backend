import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { ConfigModule } from "@nestjs/config";
import { CompanyModule } from "./company/company.module";
import TypeOrmConfigService from "./persistance/typeorm-config.service";
import { MulterModule } from "@nestjs/platform-express";
import { DepartmentModule } from './department/department.module';
import { TypeOrmModule } from "@nestjs/typeorm";
import { PositionModule } from './position/position.module';
import { UserModule } from './user/user.module';

@Module({
    imports: [
        ConfigModule.forRoot({ isGlobal: true, envFilePath: ".env" }),
        TypeOrmModule.forRootAsync({
            useClass: TypeOrmConfigService
        }),
        CompanyModule,
        MulterModule.register({
            dest: "./files"
        }),
        DepartmentModule,
        PositionModule,
        UserModule
    ],
    controllers: [AppController],
    providers: [AppService]
})
export class AppModule {
}
