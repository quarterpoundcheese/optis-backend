import * as fs from "fs";
import { v4 as uuidv4 } from "uuid";
import * as path from "path";

export async function saveFile(file: Express.Multer.File, allowedMimetypes?: string[]): Promise<string> {
    const fileName = uuidv4();
    return new Promise((resolve, reject) => {

        if(allowedMimetypes && !allowedMimetypes.includes(file.mimetype)) reject({message: "This mimetype is not allowed"})

        fs.writeFile(path.join(__dirname, "../../files/" + fileName + "." + file.originalname.split(".")[1]), file.buffer, (err) => {
            if (err) {
                return reject(err);
            }
            resolve(fileName);
        });
    });
}