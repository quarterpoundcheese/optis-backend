import { ValidationError } from "class-validator";

export default function validationHelper(errors: ValidationError[]):any {
    const keyValueErrors = {};
    errors.forEach((e) => {
        keyValueErrors[e.property] = Object.values(e.constraints);
    })
    return keyValueErrors
}