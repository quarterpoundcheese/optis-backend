import {
    AfterLoad,
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity, getRepository,
    ManyToOne, OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import { Company } from "../../company/entities/company.entity";
import { IsNotEmpty, Length } from "class-validator";
import { Position } from "../../position/entities/position.entity";

@Entity("departments")
export class Department {

    @PrimaryGeneratedColumn()
    id: number;

    @Length(2, 30, { message: "" })
    @IsNotEmpty({ message: "" })
    @Column()
    name: string;

    @IsNotEmpty({ message: "" })
    @ManyToOne(_ => Company, (company) => company.departments)
    company: Company;

    @AfterLoad()
    async calculateDeletable() {
        const t = await getRepository(Position).count({ where: { department: this } });
        this.isDeleteAble = t === 0;
    }

    isDeleteAble: boolean;


    @OneToMany(_ => Position, p => p)
    positions: Position[];

    @CreateDateColumn()
    created!: Date;

    @UpdateDateColumn()
    updated!: Date;

    // Add this column to your entity!
    @DeleteDateColumn()
    deletedAt?: Date;
}
