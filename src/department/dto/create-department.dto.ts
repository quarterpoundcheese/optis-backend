import { IsNotEmpty, Length } from "class-validator";

export class CreateDepartmentDto {
    @IsNotEmpty()
    @Length(2, 18)
    name: string
    @IsNotEmpty()
    companyId: number;
}
