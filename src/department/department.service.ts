import { BadRequestException, HttpException, HttpStatus, Injectable, NotFoundException } from "@nestjs/common";
import { CreateDepartmentDto } from "./dto/create-department.dto";
import { UpdateDepartmentDto } from "./dto/update-department.dto";
import { Connection, Repository } from "typeorm";
import { Department } from "./entities/department.entity";
import { Company } from "../company/entities/company.entity";
import { validate } from "class-validator";
import validationHelper from "../helpers/validation.helper";

@Injectable()
export class DepartmentService {
    private _departmentRepository: Repository<Department>;
    private _companyRepository: Repository<Company>;

    constructor(private _connection: Connection) {
        this._companyRepository = this._connection.getRepository(Company);
        this._departmentRepository = this._connection.getRepository(Department);
    }

    async create(createDepartmentDto: CreateDepartmentDto) {

        const validationErrors = await validate(createDepartmentDto);
        if (validationErrors.length > 0) throw new HttpException(validationHelper(validationErrors), HttpStatus.BAD_REQUEST);

        const company = await this._companyRepository.findOne(createDepartmentDto.companyId);
        if (!company) throw new NotFoundException();


        const department = this._departmentRepository.create({ ...createDepartmentDto, company });
        await this._departmentRepository.save(department);
        return department;
    }

    findAll() {
        return this._departmentRepository.createQueryBuilder("d")
            .select(["d", "company.id", "company.name"])
            .leftJoin("d.company", "company")
            .getMany();
    }


    findOne(id: number) {
        return this._departmentRepository.createQueryBuilder("d")
            .select(["d", "company.id", "company.name"])
            .where({id})
            .leftJoin("d.company", "company")
            .getOne();
    }

    async update(id: number, updateDepartmentDto: UpdateDepartmentDto) {
        const validationErrors = await validate(updateDepartmentDto);
        if (validationErrors.length > 0) throw new HttpException(validationHelper(validationErrors), HttpStatus.BAD_REQUEST);

        const department = await this._departmentRepository.findOne(id, {relations: ["company"]})
        if (!department) throw new NotFoundException();


        const company = await this._companyRepository.findOne(updateDepartmentDto.companyId);
        if (!company) throw new NotFoundException();


        const updateDepartment = this._departmentRepository.create({ ...department, ...updateDepartmentDto, company });
        await this._departmentRepository.save(updateDepartment);
        return updateDepartment;
    }

    async remove(id: number) {
        const department = await this._departmentRepository.findOne(id);
        if(!department) throw new NotFoundException();
        if(!department.isDeleteAble) throw new BadRequestException();

        await this._departmentRepository.softDelete({id: department.id})
    }
}
